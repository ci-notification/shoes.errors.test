﻿using System;

namespace Shoes.Errors.Exceptions.NotificationServiceClient
{
	public class NotificationServiceClientException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.NotificationServiceClientGeneral; }
		}

		#endregion

		#region constructors

		public NotificationServiceClientException()
		{

		}

		public NotificationServiceClientException(string message)
			: base(message)
		{
		}

		public NotificationServiceClientException(string message, Exception inner)
			: base(message, inner)
		{
		}

		#endregion
	}
}
