﻿using System;
using System.Web.Http.ModelBinding;

namespace Shoes.Errors.Exceptions
{
    [Serializable]
    public class InvalidDataModelException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.InvalidDataModel; }
        }

        public ModelStateDictionary ModelStateDictionary { get; private set; }

        #endregion

        #region constructors

        public InvalidDataModelException()
        {

        }

        public InvalidDataModelException(string message) 
            : base(message)
        {
        }

        public InvalidDataModelException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        public InvalidDataModelException(ModelStateDictionary model)
        {
            ModelStateDictionary = model;
        }

        #endregion

    }
}


