﻿using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	public class InvalidItemInsertInputException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidItemInsertInput; }
		}

		#endregion

		#region constructors

		public InvalidItemInsertInputException()
		{

		}

		public InvalidItemInsertInputException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public InvalidItemInsertInputException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}
		#endregion
	}
}
