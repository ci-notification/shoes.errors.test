﻿using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	/// <summary>
	/// 
	/// </summary>
	/// <seealso cref="Shoes.Errors.Exceptions.ShoesException" />
	[Serializable]
	public class ProductException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.ProductGeneral; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductException"/> class.
		/// </summary>
		public ProductException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public ProductException(string message)
			: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public ProductException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion

	}

}
