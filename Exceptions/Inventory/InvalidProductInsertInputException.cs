﻿
using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	public class InvalidProductInsertInputException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidProductInsertInput; }
		}

		#endregion

		#region constructors

		public InvalidProductInsertInputException()
		{

		}

		public InvalidProductInsertInputException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public InvalidProductInsertInputException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}
		#endregion
	}
}
