﻿using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	public class InvalidBrandInsertException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidBrandInsertInput; }
		}

		#endregion

		#region constructors

		public InvalidBrandInsertException()
		{

		}

		public InvalidBrandInsertException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public InvalidBrandInsertException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}
		#endregion
	}
}
