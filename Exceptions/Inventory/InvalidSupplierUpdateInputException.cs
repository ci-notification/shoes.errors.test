﻿
using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	public class InvalidSupplierUpdateInputException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidSupplierUpdateInput; }
		}

		#endregion

		#region constructors

		public InvalidSupplierUpdateInputException()
		{

		}

		public InvalidSupplierUpdateInputException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public InvalidSupplierUpdateInputException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}
		#endregion
	}
}
