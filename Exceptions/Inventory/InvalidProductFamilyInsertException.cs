﻿
using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	public class InvalidProductFamilyInsertException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidProductFamilyInsertInput; }
		}

		#endregion

		#region constructors

		public InvalidProductFamilyInsertException()
		{

		}

		public InvalidProductFamilyInsertException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public InvalidProductFamilyInsertException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}
		#endregion
	}
}
