﻿using System;

namespace Shoes.Errors.Exceptions.Inventory
{
	[Serializable]
	public class InventoryException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InventoryGeneral; }
		}

		#endregion

		#region constructors

		public InventoryException()
		{

		}

		public InventoryException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public InventoryException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}
		#endregion
	}
}
