﻿using System;

namespace Shoes.Errors.Exceptions.CommonProject
{
	public class InvalidSalesOrderHistoryStepCodeException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode 
		{
			get { return ErrorCodeEnum.InvalidSalesOrderHistoryStepCode; }
		}

		#endregion

		#region constructors

		public InvalidSalesOrderHistoryStepCodeException()
		{

		}

		public InvalidSalesOrderHistoryStepCodeException(string message) 
            : base(message)
        {
		}

		public InvalidSalesOrderHistoryStepCodeException(string message, Exception inner) 
            : base(message, inner)
        {
		}
		#endregion
	}
}
