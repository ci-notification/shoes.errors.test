﻿using System;

namespace Shoes.Errors.Exceptions.PurchaseOrder
{
    [Serializable]
    public class PurchaseOrderException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.PurchaseOrderGeneral; }
        }

        #endregion

        #region constructors

        public PurchaseOrderException()
        {

        }

        public PurchaseOrderException(string message)
            : base(message)
        {
        }

        public PurchaseOrderException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion

    }
}
