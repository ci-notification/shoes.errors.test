﻿using System;

namespace Shoes.Errors.Exceptions.PurchaseOrder
{
	public class SupplierInsertException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SupplierInsertException; }
		}

		#endregion

		#region constructors

		public SupplierInsertException()
		{

		}

		public SupplierInsertException(string message)
            : base(message)
        {
		}

		public SupplierInsertException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
