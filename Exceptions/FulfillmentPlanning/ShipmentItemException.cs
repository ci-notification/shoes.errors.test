﻿using System;

namespace Shoes.Errors.Exceptions.FulfillmentPlanning
{
    [Serializable]
    public class ShipmentItemException : ShoesException
    {
        #region Properties

        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.FulfillmentShipmentItem; }
        }

        #endregion

        #region constructors

        public ShipmentItemException()
        {

        }

        public ShipmentItemException(string message) 
            : base(message)
        {
            LongDescription = message;
        }

        public ShipmentItemException(string message, Exception inner) 
            : base(message, inner)
        {
            LongDescription = message;
        }

        #endregion
    }
}


