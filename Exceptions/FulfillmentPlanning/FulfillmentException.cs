﻿using System;

namespace Shoes.Errors.Exceptions.FulfillmentPlanning
{
    [Serializable]
    public class FulfillmentException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.FulfillmentServiceClientGeneral; }
        }
        
        #endregion  

        #region constructors

        public FulfillmentException()
        {

        }

        public FulfillmentException(string message) 
            : base(message)
        {
        }

        public FulfillmentException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        #endregion
        
    }
}


