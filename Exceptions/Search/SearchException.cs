﻿using System;

namespace Shoes.Errors.Exceptions.Search
{
	[Serializable]
	public class SearchException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderCustomerGeneral; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SalesOrderCustomerException"/> class.
		/// </summary>
		public SearchException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SalesOrderCustomerException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public SearchException(string message)
			: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SalesOrderCustomerException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public SearchException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion

	}

}
