﻿using System;

namespace Shoes.Errors.Exceptions.FulfillmentPlanClient
{
	public class FulfillmentPlanClientException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.FulfillmentGeneral; }
		}

		#endregion

		#region constructors

		public FulfillmentPlanClientException()
		{

		}

		public FulfillmentPlanClientException(string message) 
            : base(message)
        {
		}

		public FulfillmentPlanClientException(string message, Exception inner) 
            : base(message, inner)
        {
		}

		#endregion
	}
}
