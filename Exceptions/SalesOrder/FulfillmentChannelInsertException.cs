﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	public class FulfillmentChannelInsertException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.FulfillmentChannelInsertException; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FulfillmentChannelException"/> class.
		/// </summary>
		public FulfillmentChannelInsertException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FulfillmentChannelException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public FulfillmentChannelInsertException(string message)
			: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FulfillmentChannelException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public FulfillmentChannelInsertException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion
	}
}
