﻿using System;

namespace Shoes.Errors.Exceptions.SaleOrder
{
	/// <summary>
	/// 
	/// </summary>
	/// <seealso cref="Shoes.Errors.Exceptions.ShoesException" />
	[Serializable]
	public class ShippingCodeException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.ShippingCodeGeneral; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ShippingCodeException"/> class.
		/// </summary>
		public ShippingCodeException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ShippingCodeException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public ShippingCodeException(string message)
					: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ShippingCodeException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public ShippingCodeException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion

	}

}
