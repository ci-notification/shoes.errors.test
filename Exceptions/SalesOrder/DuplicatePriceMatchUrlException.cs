﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	public class DuplicatePriceMatchUrlException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.DuplicatePriceMatchUrl; }
		}

		#endregion

		#region constructors

		public DuplicatePriceMatchUrlException()
		{

		}

		public DuplicatePriceMatchUrlException(string message) 
            : base(message)
        {
		}

		public DuplicatePriceMatchUrlException(string message, Exception inner) 
            : base(message, inner)
        {
		}

		#endregion
	}
}
