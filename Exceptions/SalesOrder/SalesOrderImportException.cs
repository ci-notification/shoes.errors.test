﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	[Serializable]
	public class SalesOrderImportException  : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderImportFaliure; }
		}

		#endregion

		#region constructors

		public SalesOrderImportException()
		{

		}

		public SalesOrderImportException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public SalesOrderImportException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}

		#endregion
	}
}
