﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
    [Serializable]
    public class PriceAdjustmentException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.PriceAdjustment; }
        }
        
        #endregion  

        #region constructors

        public PriceAdjustmentException()
        {

        }

        public PriceAdjustmentException(string message) 
            : base(message)
        {
        }

        public PriceAdjustmentException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        #endregion
        
    }
}


