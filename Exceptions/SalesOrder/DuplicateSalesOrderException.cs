﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
    [Serializable]
    public class DuplicateSalesOrderException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.SalesOrderDuplicate; }
        }
        
        #endregion  

        #region constructors

        public DuplicateSalesOrderException()
        {

        }

        public DuplicateSalesOrderException(string message) 
            : base(message)
        {
        }

        public DuplicateSalesOrderException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        #endregion
        
    }
}


