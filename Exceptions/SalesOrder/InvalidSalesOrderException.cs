﻿using System;
using System.Collections.Generic;

namespace Shoes.Errors.Exceptions.SalesOrder
{
    [Serializable]
    public class InvalidSalesOrderException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.InvalidSalesOrder; }
        }

        public object SalesOrderDTO { get; set; }
        public List<string> Errors { get; set; }

        #endregion

        #region constructors

        public InvalidSalesOrderException()
        {

        }

        public InvalidSalesOrderException(string message) 
            : base(message)
        {
        }

        public InvalidSalesOrderException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        public InvalidSalesOrderException(object salesOrderDTO, List<string> errors)
        {
            SalesOrderDTO = salesOrderDTO;
            Errors = errors;
        }

        public InvalidSalesOrderException(object salesOrderDTO, Exception inner, string message)
            : base(message, inner)
        {
            SalesOrderDTO = salesOrderDTO;
            LongDescription = message;
        }

        #endregion

    }
}


