﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	[Serializable]
	public class PriceMatchUrlException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.PriceMatchUrlException; }
		}
		#endregion

		#region constructors

		public PriceMatchUrlException()
		{

		}

		public PriceMatchUrlException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public PriceMatchUrlException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}

		#endregion
	}
}
