﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	[Serializable]
	public class SalesOrderItemUpdateException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderItemUpdateException; }
		}

		#endregion

		#region constructors

		public SalesOrderItemUpdateException()
		{

		}

		public SalesOrderItemUpdateException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public SalesOrderItemUpdateException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}

		#endregion

	}
}
