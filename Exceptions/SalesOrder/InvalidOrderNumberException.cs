﻿using System;
using System.Collections.Generic;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	[Serializable]
	public class InvalidOrderNumberException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidSalesOrder; }
		}

		public object SalesOrderDTO { get; set; }
		public List<string> Errors { get; set; }

		#endregion

		#region constructors

		public InvalidOrderNumberException()
		{

		}

		public InvalidOrderNumberException(string message) 
            : base(message)
        {
		}

		public InvalidOrderNumberException(string message, Exception inner) 
            : base(message, inner)
        {
		}

		#endregion
	}
}
