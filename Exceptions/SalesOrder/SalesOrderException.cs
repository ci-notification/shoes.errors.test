﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	[Serializable]
	public class SalesOrderException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderGeneral; }
		}

		#endregion

		#region constructors

		public SalesOrderException()
		{

		}

		public SalesOrderException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public SalesOrderException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}

		#endregion

	}
}
