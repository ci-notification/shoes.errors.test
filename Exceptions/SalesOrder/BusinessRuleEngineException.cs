﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	public class BusinessRuleEngineException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.BusinessRuleEngineException; }
		}

		#endregion

		#region constructors

		public BusinessRuleEngineException()
		{

		}

		public BusinessRuleEngineException(string message) 
            : base(message)
        {
		}

		public BusinessRuleEngineException(string message, Exception inner) 
            : base(message, inner)
        {
		}

		#endregion
	}
}
