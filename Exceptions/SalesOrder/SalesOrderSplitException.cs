﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	[Serializable]
	public class SalesOrderSplitException : ShoesException
	{
		#region Properties

		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderGeneral; }
		}

		#endregion

		#region constructors

		public SalesOrderSplitException()
		{

		}

		public SalesOrderSplitException(string message) 
            : base(message)
        {
			LongDescription = message;
		}

		public SalesOrderSplitException(string message, Exception inner) 
            : base(message, inner)
        {
			LongDescription = message;
		}

		#endregion

	}
}
