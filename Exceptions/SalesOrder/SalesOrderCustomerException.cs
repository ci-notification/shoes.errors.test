﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrder
{
	/// <summary>
	/// 
	/// </summary>
	/// <seealso cref="Shoes.Errors.Exceptions.ShoesException" />
	[Serializable]
	public class SalesOrderCustomerException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderCustomerGeneral; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SalesOrderCustomerException"/> class.
		/// </summary>
		public SalesOrderCustomerException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SalesOrderCustomerException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public SalesOrderCustomerException(string message)
			: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SalesOrderCustomerException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public SalesOrderCustomerException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion

	}

}
