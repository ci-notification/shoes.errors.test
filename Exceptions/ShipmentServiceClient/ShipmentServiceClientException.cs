﻿using System;

namespace Shoes.Errors.Exceptions.ShipmentServiceClient
{
	[Serializable]
	public class ShipmentServiceClientException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.ShipmentServiceClientGeneral; }
		}

		#endregion

		#region constructors

		public ShipmentServiceClientException()
		{

		}

		public ShipmentServiceClientException(string message)
			: base(message)
		{
		}

		public ShipmentServiceClientException(string message, Exception inner)
			: base(message, inner)
		{
		}

		#endregion
	}
}
