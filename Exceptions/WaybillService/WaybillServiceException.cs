﻿using System;

namespace Shoes.Errors.Exceptions.WaybillService
{
    [Serializable]
    public class WaybillServiceException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.WaybillGeneral; }
        }
        
        #endregion  

        #region constructors

        public WaybillServiceException()
        {

        }

        public WaybillServiceException(string message) 
            : base(message)
        {
        }

        public WaybillServiceException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        #endregion
        
    }
}


