﻿using System;

namespace Shoes.Errors.Exceptions.WaybillService
{
    [Serializable]
    public class WaybillRuleException : WaybillServiceException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.WaybillRuleException; }
        }
        
        #endregion  

        #region constructors

        public WaybillRuleException()
        {

        }

        public WaybillRuleException(string message) 
            : base(message)
        {
        }

        public WaybillRuleException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        public WaybillRuleException(int salesChannelID, string carrier, string countryCode) : base("No valid rules found.")
        {
            LongDescription = $"No valid rules found given the following parameters: SalesChannelID: [{salesChannelID}], Carrier: [{carrier}], CountryCode: [{countryCode}]";
        }
        #endregion
        
    }
}


