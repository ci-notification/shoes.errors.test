﻿using System;

namespace Shoes.Errors.Exceptions.WaybillService
{
    [Serializable]
    public class WaybillGenerateException : WaybillServiceException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.WaybillDownloadException; }
        }
        
        #endregion  

        #region constructors

        public WaybillGenerateException()
        {

        }

        public WaybillGenerateException(string message) 
            : base(message)
        {
        }

        public WaybillGenerateException(string message, Exception inner) 
            : base(message, inner)
        {
        }
        
        #endregion
        
    }
}


