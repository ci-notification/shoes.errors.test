﻿using System;

namespace Shoes.Errors.Exceptions.WaybillService
{
    [Serializable]
    public class WaybillDownloadException : WaybillServiceException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.WaybillDownloadException; }
        }
        
        #endregion  

        #region constructors

        public WaybillDownloadException()
        {

        }

        public WaybillDownloadException(string message) 
            : base(message)
        {
        }

        public WaybillDownloadException(string message, Exception inner) 
            : base(message, inner)
        {
        }
        
        #endregion
        
    }
}


