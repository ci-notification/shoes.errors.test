﻿namespace Shoes.Errors.Exceptions
{
    public interface IShoesException
    {
        /// <summary>
        /// Should be hardcoded in the custom exception
        /// </summary>
        ErrorCodeEnum ErrorCode { get; }

        /// <summary>
        /// Should be hardcoded in the ErrorCodes.  This is a standard error message for all occurances;
        /// </summary>
        string ShortDescription { get; }

        /// <summary>
        /// Descriptive message that isnt customer facing, more useful for debugging what went wrong
        /// </summary>
        string LongDescription { get; set; }
    }
}
