﻿using System;

namespace Shoes.Errors.Exceptions.SalesOrderServiceClient
{
	[Serializable]
	public class SalesOrderServiceClientException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SalesOrderServiceClientGeneral; }
		}

		#endregion

		#region constructors

		public SalesOrderServiceClientException()
		{

		}

		public SalesOrderServiceClientException(string message)
            : base(message)
        {
		}

		public SalesOrderServiceClientException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
