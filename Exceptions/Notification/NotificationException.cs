﻿using System;

namespace Shoes.Errors.Exceptions.Notification
{
    [Serializable]
    public class NotificationException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.NotificationGeneral; }
        }

        #endregion

        #region constructors

        public NotificationException()
        {

        }

        public NotificationException(string message)
            : base(message)
        {
        }

        public NotificationException(string message, Exception inner)
            : base(message, inner)
        {
        }

        #endregion

    }
}
