﻿using System;

namespace Shoes.Errors.Exceptions.PurchaseOrderServiceClient
{
	[Serializable]
	public class PurchaseOrderServiceClientException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.PurchaseOrderServiceClientGeneral; }
		}

		#endregion

		#region constructors

		public PurchaseOrderServiceClientException()
		{

		}

		public PurchaseOrderServiceClientException(string message)
			: base(message)
		{
		}

		public PurchaseOrderServiceClientException(string message, Exception inner)
			: base(message, inner)
		{
		}

		#endregion
	}
}
