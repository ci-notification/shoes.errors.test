﻿using Shoes.WebServicesAPI.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Http.ModelBinding;

namespace Shoes.Errors.Exceptions
{
    [Serializable]
    public class FailedAPICallException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.FailedAPICall; }
        }

        public IEnumerable<ErrorDTO> ErrorDTOs { get; set; }
        public string Resource { get; set; }
        public string Body { get; set; }
        public string Method { get; set; }
        
        #endregion

        #region constructors

        public FailedAPICallException(IEnumerable<ErrorDTO> errors, string resource, string method, string body,string msg = null) 
        {
            ErrorDTOs = errors;
            Resource = resource;
            Body = body;
            Method = method;
            SetLongDescription(msg);
        }

        public FailedAPICallException(string resource, string method, string body, string msg = null)
        {
            Resource = resource;
            Method = method;
            Body = body;
            SetLongDescription(msg);
        }
		#endregion

		private void SetLongDescription(string msg)
        {
			string partialMsg = $"Failed making API Call to Resource: {Resource}, Method: {Method}, Body: {Body}.";
			LongDescription = string.IsNullOrWhiteSpace(msg) ? partialMsg : partialMsg + msg;
		}
    }
}


