﻿using System;

namespace Shoes.Errors.Exceptions.MessageSecurity
{
    [Serializable]
    public class MessageSecurityException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.InvalidMessageHash; }
        }
        
        #endregion  

        #region constructors

        public MessageSecurityException()
        {

        }

        public MessageSecurityException(string message) 
            : base(message)
        {
        }

        public MessageSecurityException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        #endregion
        
    }
}


