﻿
using System;

namespace Shoes.Errors.Exceptions.Return
{
	[Serializable]
	public class InvalidReturnReasonCodeException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidReturnReasonCodeException; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ReturnException"/> class.
		/// </summary>
		public InvalidReturnReasonCodeException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReturnException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public InvalidReturnReasonCodeException(string message)
			: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReturnException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public InvalidReturnReasonCodeException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion
	}
}
