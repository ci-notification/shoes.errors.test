﻿using System;

namespace Shoes.Errors.Exceptions.Return
{
	/// <summary>
	/// 
	/// </summary>
	/// <seealso cref="Shoes.Errors.Exceptions.ShoesException" />
	[Serializable]
	public class ReturnException : ShoesException
	{
		#region Properties

		/// <summary>
		/// Gets the error code.
		/// </summary>
		/// <value>
		/// The error code.
		/// </value>
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.ReturnGeneral; }
		}

		#endregion

		#region constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ReturnException"/> class.
		/// </summary>
		public ReturnException()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReturnException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public ReturnException(string message)
			: base(message)
		{
			LongDescription = message;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReturnException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public ReturnException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion

	}

}
