﻿using System;

namespace Shoes.Errors.Exceptions.InventoryServiceClient
{
	[Serializable]
	public class InventoryServiceClientException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InventoryServiceClientGeneral; }
		}

		#endregion

		#region constructors

		public InventoryServiceClientException()
		{

		}

		public InventoryServiceClientException(string message)
			: base(message)
		{
		}

		public InventoryServiceClientException(string message, Exception inner)
			: base(message, inner)
		{
		}

		#endregion
	}
}
