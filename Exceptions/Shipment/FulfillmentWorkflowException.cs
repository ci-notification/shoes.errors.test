﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
    [Serializable]
    public class FulfillmentWorkflowException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.FulfillmentWorkflow; }
        }
        
        #endregion  

        #region constructors

        public FulfillmentWorkflowException()
        {

        }

        public FulfillmentWorkflowException(string message) 
            : base(message)
        {
        }

        public FulfillmentWorkflowException(string message, Exception inner) 
            : base(message, inner)
        {
        }
        #endregion
        
    }
}


