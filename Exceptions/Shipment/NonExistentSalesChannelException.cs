﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	public class NonExistentSalesChannelException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.NonExistentSalesChannel; }
		}

		#endregion

		#region constructors

		public NonExistentSalesChannelException()
		{

		}

		public NonExistentSalesChannelException(string message)
            : base(message)
        {
		}

		public NonExistentSalesChannelException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
