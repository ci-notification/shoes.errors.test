﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	public class UnallowedFulfillmentRequestStatusException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.UnallowedFulfillmentRequestStatus; }
		}

		#endregion

		#region constructors

		public UnallowedFulfillmentRequestStatusException()
		{

		}

		public UnallowedFulfillmentRequestStatusException(string message)
            : base(message)
        {
		}

		public UnallowedFulfillmentRequestStatusException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
