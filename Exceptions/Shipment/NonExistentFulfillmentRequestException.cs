﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	public class NonExistentFulfillmentRequestException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.NonExistentFulfillmentRequest; }
		}

		#endregion

		#region constructors

		public NonExistentFulfillmentRequestException()
		{

		}

		public NonExistentFulfillmentRequestException(string message)
            : base(message)
        {
		}

		public NonExistentFulfillmentRequestException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
