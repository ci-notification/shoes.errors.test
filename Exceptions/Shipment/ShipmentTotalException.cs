﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	[Serializable]
	public class ShipmentTotalException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.ShipmentTotal; }
		}

		#endregion

		#region constructors

		public ShipmentTotalException()
		{

		}

		public ShipmentTotalException(string message)
            : base(message)
        {
		}

		public ShipmentTotalException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
