﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
    [Serializable]
    public class NoFulfillmentWorkflowException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.NoFulfillmentWorkflow; }
        }

        public object FulfillmentRequestBLTO { get; set; }
        #endregion  

        #region constructors

        public NoFulfillmentWorkflowException()
        {

        }

        public NoFulfillmentWorkflowException(string message) 
            : base(message)
        {
        }

        public NoFulfillmentWorkflowException(string message, Exception inner) 
            : base(message, inner)
        {
        }
        public NoFulfillmentWorkflowException(object fulfillmentRequestBLTO)
        {
            FulfillmentRequestBLTO = fulfillmentRequestBLTO;
        }

        #endregion

    }
}


