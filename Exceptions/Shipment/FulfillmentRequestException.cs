﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	[Serializable]
	public class InvalidFulfillmentRequestException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.InvalidFulfillmentRequest; }
		}

		#endregion

		#region constructors

		public InvalidFulfillmentRequestException()
		{

		}

		public InvalidFulfillmentRequestException(string message)
            : base(message)
        {
		}

		public InvalidFulfillmentRequestException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
