﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	public class NonExistentFulfillmentRequestItemException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.NonExistentFulfillmentRequest; }
		}

		#endregion

		#region constructors

		public NonExistentFulfillmentRequestItemException()
		{

		}

		public NonExistentFulfillmentRequestItemException(string message)
            : base(message)
        {
		}

		public NonExistentFulfillmentRequestItemException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
