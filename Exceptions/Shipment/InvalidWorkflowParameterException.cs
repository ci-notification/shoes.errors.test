﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
    [Serializable]
    public class InvalidWorkflowParameterException : ShoesException
    {
        #region properties
        public override ErrorCodeEnum ErrorCode
        {
            get { return ErrorCodeEnum.InvalidFulfillmentWorkflowParameter; }
        }

        public object FulfillmentRequestBLTO { get; set; }

        public object FulfillmentWorkflowParameterValue { get; set; }

        #endregion

        #region constructors

        public InvalidWorkflowParameterException()
        {

        }

        public InvalidWorkflowParameterException(string message) 
            : base(message)
        {
        }

        public InvalidWorkflowParameterException(object fulfillmentRequestBLTO, object workflowParameterValue)
        {
            FulfillmentRequestBLTO = fulfillmentRequestBLTO;
            FulfillmentWorkflowParameterValue = workflowParameterValue;
        }

        #endregion
    }
}


