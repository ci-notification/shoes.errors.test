﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	[Serializable]
	public class ShipmentException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.ShipmentGeneral; }
		}

		#endregion

		#region constructors

		public ShipmentException()
		{

		}

		public ShipmentException(string message)
            : base(message)
        {
		}

		public ShipmentException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
