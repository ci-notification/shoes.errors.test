﻿using System;

namespace Shoes.Errors.Exceptions.Shipment
{
	public class NonExistentSalesOrderItemException : ShoesException
	{
		#region properties
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.NonExistentSalesOrderItem; }
		}

		#endregion

		#region constructors

		public NonExistentSalesOrderItemException()
		{

		}

		public NonExistentSalesOrderItemException(string message)
            : base(message)
        {
		}

		public NonExistentSalesOrderItemException(string message, Exception inner)
            : base(message, inner)
        {
		}

		#endregion
	}
}
