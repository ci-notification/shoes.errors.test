﻿using System;

namespace Shoes.Errors.Exceptions.Sequence
{
	[Serializable]
	public class SequenceException : ShoesException
	{
		#region Properties
        
		public override ErrorCodeEnum ErrorCode
		{
			get { return ErrorCodeEnum.SequenceError; }
		}

		#endregion

		#region constructors

		public SequenceException()
		{

		}
        
		public SequenceException(string message)
			: base(message)
		{
			LongDescription = message;
		}
        
		public SequenceException(string message, Exception inner)
			: base(message, inner)
		{
			LongDescription = message;
		}

		#endregion

	}

}
