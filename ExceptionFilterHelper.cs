﻿using Shoes.WebServicesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Shoes.Errors
{
    public static class ExceptionFilterHelper
    {
        /// <summary>
        /// Registers the filters.
        /// Uses reflection to find out what current Assembly is, and find all the filters within it, then register the filters.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void RegisterFilters(HttpConfiguration config)
        {
            var filterClasses = (from type in Assembly.GetAssembly(MethodBase.GetCurrentMethod().DeclaringType).GetTypes()
                          where typeof(IExceptionFilter).IsAssignableFrom(type)
                          select type);

            foreach (var filter in filterClasses)
            {
                config.Filters.Add(Activator.CreateInstance(filter) as IExceptionFilter);
            }
        }

        public static ObjectContent CreateObjectContent(string status, object payload, IEnumerable<ErrorDTO> errors)
        {
            return new ObjectContent(typeof(APIResponseDTO),
                                        new APIResponseDTO()
                                        {
                                            Status = status,
                                            Payload = payload,
                                            Errors = errors
                                        },
                                        new JsonMediaTypeFormatter()
                                    );
        }

        public static ObjectContent CreateObjectContent(string status, IEnumerable<ErrorDTO> errors)
        {
            return CreateObjectContent(status, string.Empty, errors);
        }

        public static HttpResponseMessage GetHttpResponse(HttpRequestMessage requestMessage, string reasonPhrase, IEnumerable<ErrorDTO> errors, HttpStatusCode httpStatusCode)
        {
            return new HttpResponseMessage(httpStatusCode)
            {
                Content = ExceptionFilterHelper.CreateObjectContent("error", errors),
                RequestMessage = requestMessage,
                ReasonPhrase = reasonPhrase
            };
        }

        public static HttpResponseMessage GetHttpResponse(HttpRequestMessage requestMessage, string reasonPhrase, IEnumerable<ErrorDTO> errors)
        {
            return GetHttpResponse(requestMessage, reasonPhrase, errors, HttpStatusCode.InternalServerError);
        }
    }
}
