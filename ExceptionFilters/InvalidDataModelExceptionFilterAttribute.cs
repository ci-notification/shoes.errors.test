﻿using Shoes.Errors.Exceptions;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters
{
    public class InvalidDataModelExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is InvalidDataModelException)
            {
                var exception = actionExecutedContext.Exception as InvalidDataModelException;

                var errorsMessages = new List<string>();

                //sometimes exceptions are thrown (in the case of required)
                errorsMessages.AddRange(exception.ModelStateDictionary.Values
                    .SelectMany(val => val.Errors).Where(e=>e.Exception != null)
                    .Select(e => e?.Exception?.Message));

                //if its a validiation error its not an exception
                errorsMessages.AddRange(exception.ModelStateDictionary.Values
                    .SelectMany(val => val.Errors).Where(e=>e.Exception == null)
                    .Select(e => e?.ErrorMessage));

                var errors = new List<ErrorDTO>();
                var error = new ErrorDTO()
                {
                    ErrorCode = (int)exception.ErrorCode,
                    ShortDescription = exception.ShortDescription,
                    LongDescription = exception.LongDescription,
                    //get the error messages only, not all the other junk
                    ErrorDetails = errorsMessages
                };
                errors.Add(error);
                
                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
            }

        }
    }
}
