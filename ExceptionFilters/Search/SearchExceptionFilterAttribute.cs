﻿using Shoes.Errors.Exceptions.Search;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.Search
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ExceptionFilterAttribute" />
    /// <seealso cref="System.Web.Http.Filters.IExceptionFilter" />
    public class SearchExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
	{
		/// <summary>
		/// Called when [exception].
		/// </summary>
		/// <param name="actionExecutedContext">The action executed context.</param>
		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception is SearchException)
			{
				var exception = actionExecutedContext.Exception as SearchException;
				var errors = new List<ErrorDTO>();
				var error = new ErrorDTO()
				{
					ErrorCode = (int)exception.ErrorCode,
					ShortDescription = exception.ShortDescription,
					LongDescription = exception.LongDescription,
					ErrorDetails = exception?.InnerException?.Message
				};
				errors.Add(error);

				actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
			}
		}
	}
}
