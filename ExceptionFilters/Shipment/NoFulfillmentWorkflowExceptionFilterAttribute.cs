﻿using Shoes.Errors.Exceptions.Shipment;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.Shipment
{
	public class NoFulfillmentWorkflowExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is NoFulfillmentWorkflowException)
            {
                var exception = actionExecutedContext.Exception as NoFulfillmentWorkflowException;
                var errors = new List<ErrorDTO>();
                var error = new ErrorDTO()
                {
                    ErrorCode = (int)exception.ErrorCode,
                    ShortDescription = exception.ShortDescription,
                    LongDescription = exception.LongDescription,
                    ErrorDetails = exception.FulfillmentRequestBLTO
                };
                errors.Add(error);

                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
            }
        }
    }
}
