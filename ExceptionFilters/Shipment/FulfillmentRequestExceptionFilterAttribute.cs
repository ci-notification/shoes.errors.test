﻿using Shoes.Errors.Exceptions.Shipment;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.Shipment
{
	public class FulfillmentRequestExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
	{

		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception is InvalidFulfillmentRequestException)
			{
				var exception = actionExecutedContext.Exception as InvalidFulfillmentRequestException;
				var errors = new List<ErrorDTO>();
				var error = new ErrorDTO()
				{
					ErrorCode = (int)exception.ErrorCode,
					ShortDescription = exception.ShortDescription,
					LongDescription = exception.LongDescription,
					ErrorDetails = string.Empty
				};
				errors.Add(error);

				actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
			}
		}
	}
}
