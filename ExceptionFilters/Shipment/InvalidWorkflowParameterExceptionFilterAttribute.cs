﻿using Shoes.Errors.Exceptions.Shipment;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.Shipment
{
	public class InvalidWorkflowParameterExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is InvalidWorkflowParameterException)
            {
                var exception = actionExecutedContext.Exception as InvalidWorkflowParameterException;
                var errors = new List<ErrorDTO>();
                var error = new ErrorDTO()
                {
                    ErrorCode = (int)exception.ErrorCode,
                    ShortDescription = exception.ShortDescription,
                    LongDescription = exception.LongDescription,
                    ErrorDetails = new List<object>() { exception.FulfillmentRequestBLTO, exception.FulfillmentWorkflowParameterValue }
                };
                errors.Add(error);

                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
            }
        }
    }
}
