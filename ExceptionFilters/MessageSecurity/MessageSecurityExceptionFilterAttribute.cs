﻿using Shoes.Errors.Exceptions.MessageSecurity;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.MessageSecurity
{
    public class MessageSecurityExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        private const string AUTHENTICATION_SCHEME = "amx";

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is MessageSecurityException)
            {
                var exception = actionExecutedContext.Exception as MessageSecurityException;
                var errors = new List<ErrorDTO>();
                var error = new ErrorDTO()
                {
                    ErrorCode = (int)exception.ErrorCode,
                    ShortDescription = exception.ShortDescription,
                    LongDescription = exception.LongDescription,
                    ErrorDetails = string.Empty
                };
                errors.Add(error);

                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors, System.Net.HttpStatusCode.Unauthorized);
                actionExecutedContext.Response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(AUTHENTICATION_SCHEME));
            }
        }
    }
}
