﻿using Shoes.Errors.Exceptions;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters
{
    public class FailedAPICallExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is FailedAPICallException)
            {
                var exception = actionExecutedContext.Exception as FailedAPICallException;

                var errors = new List<ErrorDTO>();

                errors.Add(new ErrorDTO()
                {
                    ErrorCode = (int)exception?.ErrorCode,
                    ShortDescription = exception?.ShortDescription,
                    LongDescription = exception?.LongDescription

                });

                if(exception?.ErrorDTOs != null)
				{
					errors.AddRange(exception?.ErrorDTOs);
				}
					
                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
            }

        }
    }
}
