﻿using Shoes.Errors.Exceptions.FulfillmentPlanning;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.FulfillmentPlanning
{
    public class FulfillmentExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is FulfillmentException)
            {
                var exception = actionExecutedContext.Exception as FulfillmentException;
                var errors = new List<ErrorDTO>();
                var error = new ErrorDTO()
                {
                    ErrorCode = (int)exception.ErrorCode,
                    ShortDescription = exception.ShortDescription,
                    LongDescription = exception.LongDescription,
                    ErrorDetails = exception?.InnerException?.Message
				};
                errors.Add(error);

                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
            }
        }
    }
}
