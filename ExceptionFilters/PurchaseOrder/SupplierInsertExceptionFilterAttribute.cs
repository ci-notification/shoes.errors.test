﻿using Shoes.Errors.Exceptions.PurchaseOrder;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.PurchaseOrder
{
	public class SupplierInsertExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
	{
		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception is SupplierInsertException)
			{
				var exception = actionExecutedContext.Exception as SupplierInsertException;
				var errors = new List<ErrorDTO>();
				var error = new ErrorDTO()
				{
					ErrorCode = (int)exception.ErrorCode,
					ShortDescription = exception.ShortDescription,
					LongDescription = exception.LongDescription,
					ErrorDetails = exception?.InnerException?.Message
				};
				errors.Add(error);

				actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
			}
		}
	}
}
