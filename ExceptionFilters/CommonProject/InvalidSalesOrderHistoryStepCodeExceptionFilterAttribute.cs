﻿using Shoes.Errors.Exceptions.CommonProject;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.Common
{
	public class InvalidSalesOrderHistoryStepCodeExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
	{
		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception is InvalidSalesOrderHistoryStepCodeException)
			{
				var exception = actionExecutedContext.Exception as InvalidSalesOrderHistoryStepCodeException;
				var errors = new List<ErrorDTO>();
				var error = new ErrorDTO()
				{
					ErrorCode = (int)exception.ErrorCode,
					ShortDescription = exception.ShortDescription,
					LongDescription = exception.LongDescription,
					ErrorDetails = exception?.InnerException?.Message
				};
				errors.Add(error);

				actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
			}
		}
	}
}
