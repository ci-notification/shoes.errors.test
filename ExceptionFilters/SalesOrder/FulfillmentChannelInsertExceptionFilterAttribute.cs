﻿using Shoes.Errors.Exceptions.SalesOrder;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.SalesOrder
{
	public class FulfillmentChannelInsertExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
	{
		/// <summary>
		/// Called when [exception].
		/// </summary>
		/// <param name="actionExecutedContext">The action executed context.</param>
		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception is FulfillmentChannelInsertException)
			{
				var exception = actionExecutedContext.Exception as FulfillmentChannelInsertException;
				var errors = new List<ErrorDTO>();
				var error = new ErrorDTO()
				{
					ErrorCode = (int)exception.ErrorCode,
					ShortDescription = exception.ShortDescription,
					LongDescription = exception.LongDescription,
					ErrorDetails = exception?.InnerException?.Message
				};
				errors.Add(error);

				actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
			}
		}
	}
}
