﻿using Shoes.Errors.Exceptions.SaleOrder;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.SalesOrder
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ExceptionFilterAttribute" />
    /// <seealso cref="System.Web.Http.Filters.IExceptionFilter" />
    public class ShippingCodeExceptionAttribute : ExceptionFilterAttribute, IExceptionFilter
	{
		/// <summary>
		/// Called when [exception].
		/// </summary>
		/// <param name="actionExecutedContext">The action executed context.</param>
		public override void OnException(HttpActionExecutedContext actionExecutedContext)
		{
			if (actionExecutedContext.Exception is ShippingCodeException)
			{
				var exception = actionExecutedContext.Exception as ShippingCodeException;
				var errors = new List<ErrorDTO>();
				var error = new ErrorDTO()
				{
					ErrorCode = (int)exception.ErrorCode,
					ShortDescription = exception.ShortDescription,
					LongDescription = exception.LongDescription,
					ErrorDetails = string.Empty
				};
				errors.Add(error);

				actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.Message, errors);
			}
		}
	}
}
