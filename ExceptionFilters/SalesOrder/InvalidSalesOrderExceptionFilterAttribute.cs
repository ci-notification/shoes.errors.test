﻿using Shoes.Errors.Exceptions.SalesOrder;
using Shoes.WebServicesAPI.Models;
using System.Collections.Generic;
using System.Web.Http.Filters;

namespace Shoes.Errors.ExceptionFilters.SalesOrder
{
    public class InvalidSalesOrderExceptionFilterAttribute : ExceptionFilterAttribute, IExceptionFilter
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is InvalidSalesOrderException)
            {
                var exception = actionExecutedContext.Exception as InvalidSalesOrderException;
                var errors = new List<ErrorDTO>();

                if (exception.Errors != null && exception.Errors.Count > 0)
                {
                    errors.Add(new ErrorDTO()
                    {
                        ErrorCode = (int)exception.ErrorCode,
                        ShortDescription = exception.ShortDescription,
                        LongDescription = exception.LongDescription,
                        ErrorDetails = exception.Errors
                    });
                }

                var error = new ErrorDTO()
                {
                    ErrorCode = (int)exception.ErrorCode,
                    ShortDescription = exception.ShortDescription,
                    LongDescription = exception?.InnerException?.Message ?? exception.LongDescription,
                    //if we have a dto object use it, otherwise see if we have an inner exception
                    ErrorDetails = exception.SalesOrderDTO ?? exception?.InnerException?.Message
                };
                errors.Add(error);

                

                actionExecutedContext.Response = ExceptionFilterHelper.GetHttpResponse(actionExecutedContext.Request, exception.ShortDescription, errors, System.Net.HttpStatusCode.BadRequest);
            }
        }
    }
}
