﻿namespace Shoes.Errors
{
    public enum ErrorCodeEnum
    {
        #region General 0000s
        General = 0001,
        InvalidMessageHash = 0011,
		SerialisationFailed = 0012,
        InvalidDataModel = 0023,
        FailedAPICall = 0030,
        #endregion

        #region Fulfillment 0100s
        FulfillmentGeneral = 0101,
        FulfillmentShipmentItem = 0102,
        #endregion

        #region SalesOrder 0200s
        SalesOrderGeneral = 0201,
        SalesOrderDuplicate = 0202,
		SalesOrderCustomerGeneral = 0203,
		FulfillmentChannelGeneral = 0204,
        InvalidSalesOrder = 0205,
		PriceMatchUrlException = 206,
		DuplicatePriceMatchUrl = 207,
        PriceAdjustment = 0208,
		SalesOrderImportFaliure = 209,
		InvalidOrderNumber = 0210,
        SalesOrderItemUpdateException = 0211,
		BusinessRuleEngineException = 0212,
		FulfillmentChannelInsertException = 0213,
		#endregion

		#region Inventory 0300s
		InventoryGeneral = 0301,
		ProductGeneral = 0302,
		InvalidSupplierUpdateInput = 0303,
		InvalidProductInsertInput = 304,
		InvalidItemInsertInput = 305,
		InvalidBrandInsertInput = 306,
		InvalidProductFamilyInsertInput = 307,
        #endregion
        
        #region Waybill 0400s
        WaybillGeneral = 0401,
        WaybillRuleException = 0402,
        WaybillDownloadException = 0403,
        WaybillGenerateException = 0404,
        #endregion

		#region Shipment 0500s
		ShipmentGeneral = 0501,
		InvalidFulfillmentRequest = 0502,
        ShipmentTotal = 0503,
		FulfillmentWorkflow = 0504,
		InvalidFulfillmentWorkflowParameter = 0505,
		NoFulfillmentWorkflow = 0506,
		NonExistentFulfillmentRequest = 0507,
		NonExistentFulfillmentRequestItem = 0508,
		UnallowedFulfillmentRequestStatus = 0509,
		NonExistentSalesOrderItem = 0510,
		NonExistentSalesChannel = 511,
		#endregion

		#region Purchase Order Service 0900s
		PurchaseOrderGeneral = 0901,
		SupplierInsertException = 0902,
		#endregion

		#region Shipping Code 1000s
		ShippingCodeGeneral = 1001,
		#endregion
		
        #region Return 1100s
		ReturnGeneral = 1101,
		InvalidReturnReasonCodeException = 1102,
		#endregion

		#region Notification 1200s
		NotificationGeneral = 1201,
        #endregion

		#region CommonProjectErrors 1300s
		InvalidSalesOrderHistoryStepCode = 1301,
		#endregion

        #region Search 1400s

        SearchGeneral = 1401,
		#endregion

		#region ShipmentServiceClient 1500s
		ShipmentServiceClientGeneral = 1501,
		#endregion

		#region SalesOrderServiceClient 1600s
		SalesOrderServiceClientGeneral = 1601,
		#endregion

		#region PurchaseOrderServiceClient 1700s
		PurchaseOrderServiceClientGeneral = 1701,
		#endregion

		#region NotificationServiceClient 1800s
		NotificationServiceClientGeneral = 1801,
		#endregion

		#region InventoryServiceClient 1900s
		InventoryServiceClientGeneral = 1901,
		#endregion

		#region SequenceService 2000s
		SequenceError = 2001,
		#endregion

		#region FulfillmentServiceClient 2100s
		FulfillmentServiceClientGeneral = 2101,
		#endregion


	}
}
