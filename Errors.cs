﻿using System;
using System.Collections.Generic;

namespace Shoes.Errors
{
    public static class Errors
    {
		//all error codes should go here
		//please keep them ordered
		static Dictionary<ErrorCodeEnum, string> _errorCodes = new Dictionary<ErrorCodeEnum, string>()
		{
            #region General
            { ErrorCodeEnum.General, "An error occured.  Please contact Support." },
			{ ErrorCodeEnum.InvalidMessageHash, "Authorization Error. Invalid Message Security Hash code provided. Please contact Support." },
			{ ErrorCodeEnum.SerialisationFailed, "Serialisation Failed at the API. Please contact Support." },
			{ ErrorCodeEnum.InvalidDataModel, "Invalid data provided for API call." },
            { ErrorCodeEnum.FailedAPICall, "Failed API call to a Shoes Web Service." },
            #endregion

            #region Fulfillment
            { ErrorCodeEnum.FulfillmentGeneral, "A Fulfillment Planning error occured.  Please contact Support." },
			{ ErrorCodeEnum.FulfillmentShipmentItem, "Failed to create shipment. Please contact Support." },
            #endregion

            #region Inventory
            { ErrorCodeEnum.InventoryGeneral, "A Inventory Fetching error occured.  Please contact Support." },
			{ ErrorCodeEnum.ProductGeneral, "A Product Fetching error occured.  Please contact Support." },
			{ ErrorCodeEnum.InvalidSupplierUpdateInput, "Trying to update supplier Failed. Please contact Support." },
			{ ErrorCodeEnum.InvalidProductInsertInput, "Trying to insert a product with invalid Data. Please contact Support." },
			{ ErrorCodeEnum.InvalidItemInsertInput, "Trying to insert an item with invalid Data. Please contact Support." },
			{ ErrorCodeEnum.InvalidBrandInsertInput, "Trying to insert a Brand with invalid Data. Please contact Support." },
			{ ErrorCodeEnum.InvalidProductFamilyInsertInput, "Trying to insert a productFamily with invalid Data. Please contact Support." },
            #endregion

            #region SalesOrder
            
            { ErrorCodeEnum.SalesOrderDuplicate, "Sales Order already exists." },
			{ ErrorCodeEnum.FulfillmentChannelGeneral, "Fulfillment Channel error occured. Please contact Support." },
			{ ErrorCodeEnum.SalesOrderCustomerGeneral, "Sales Order Customer error occurred. Please contact Support." },
			{ ErrorCodeEnum.SalesOrderGeneral, "SalesOrder error occurred. Please contact Support." },
			{ ErrorCodeEnum.ShippingCodeGeneral, "Shipping Code error occured. Please contact Support." },
			{ ErrorCodeEnum.InvalidSalesOrder, "SalesOrder is Invalid." },
            { ErrorCodeEnum.PriceAdjustment, "Price Adjustment error occured.  Please contact Support." },
			{ ErrorCodeEnum.PriceMatchUrlException, "Price Match URL error occurred. Please contact support."},
			{ ErrorCodeEnum.DuplicatePriceMatchUrl, "Trying to insert duplicate price match Url. Please contact support."},
			{ ErrorCodeEnum.SalesOrderImportFaliure, "Failed to import salesOrder. Refer error log and Sales Order Import table for more information." },
			{ ErrorCodeEnum.InvalidOrderNumber, "The OrderNumber doesn't exist for the given sales channel. Please contact support."},
            { ErrorCodeEnum.SalesOrderItemUpdateException, "SalesOrderItem Update Error occured. Please contact support."},
			{ ErrorCodeEnum.BusinessRuleEngineException, "Business Rule Engine Error occured. Please contact support."},
			{ ErrorCodeEnum.FulfillmentChannelInsertException, "FulfillmentChannel Insert Error occured. Please contact support."},
			
            #endregion
            
            #region Waybill
            { ErrorCodeEnum.WaybillGeneral, "A Waybill Service error occured. Please contact Support." },
            { ErrorCodeEnum.WaybillRuleException, "Did not find a valid rule that applies to the requested input. Cannot proceed with Waybill generation. Please contact Support." },
            { ErrorCodeEnum.WaybillDownloadException, "Could not download the Waybill file from the carrier. Cannot proceed with Waybill generation. Please contact Support." },
            { ErrorCodeEnum.WaybillGenerateException, "Could not generate the Waybill from the carrier. Please contact Support." },
            #endregion

            #region Shipping
            { ErrorCodeEnum.ShipmentGeneral, "Shipment error occurred. Please contact Support." },
            { ErrorCodeEnum.InvalidFulfillmentRequest, "Invalid FulfillmentRequest. Check Error log for details." },
            { ErrorCodeEnum.ShipmentTotal, "An error occured calculating the Shipment total.  Please contact Support." },
			{ ErrorCodeEnum.FulfillmentWorkflow, "Failed to determine Fulfillment Workflow. Please contact Support." },
			{ ErrorCodeEnum.InvalidFulfillmentWorkflowParameter, "Invalid Fulfillment Workflow Parameter. Please contact Support." },
			{ ErrorCodeEnum.NoFulfillmentWorkflow, "No Fulfillment Workflow found for Fulfillment Request. Please contact Support." },
			{ ErrorCodeEnum.NonExistentFulfillmentRequest, "An error occured Inserting Shipment in finding corresponding fulfillmentRequest.  Please contact Support." },
			{ ErrorCodeEnum.NonExistentFulfillmentRequestItem, "An error occured Inserting Shipment in finding corresponding fulfillmentRequestItem.  Please contact Support." },	
			{ ErrorCodeEnum.UnallowedFulfillmentRequestStatus, "An error occured Inserting Shipment. The corresponding FulfillmentRequest is in incorrect status.  Please contact Support." },
			{ ErrorCodeEnum.NonExistentSalesOrderItem, "An error occured Inserting Shipment in finding corresponding SalesOrderItem.  Please contact Support." },
			{ ErrorCodeEnum.NonExistentSalesChannel, "An error occured while Getting Shipping Information.  Please contact Support." },
			
            #endregion

            #region PurchaseOrder
            { ErrorCodeEnum.PurchaseOrderGeneral, "A Purchase Order Service error occured. Please contact Support." },
			{ ErrorCodeEnum.SupplierInsertException, "Trying to insert supplier Failed. Please contact Support." },
            #endregion

            #region Return
            { ErrorCodeEnum.ReturnGeneral, "A Return Service error occured. Please contact Support." },
			{ ErrorCodeEnum.InvalidReturnReasonCodeException, "Invalid Return Reason code provided. Please contact Support." },
            #endregion

            #region Notification
            { ErrorCodeEnum.NotificationGeneral, "A Notification Service error occured. Please contact Support." },
            #endregion

            #region search
            { ErrorCodeEnum.SearchGeneral, "A Search Service error occured. Please contact Support." },
            #endregion

			#region CommonProjectErrors
			{ ErrorCodeEnum.InvalidSalesOrderHistoryStepCode, "Invalid Sales Order History Code provided.  Please contact Support." },
			#endregion

			#region ShipmentServiceClient
			{ ErrorCodeEnum.ShipmentServiceClientGeneral, "Error occured at Shipment Service client. Please contact Support." },
			#endregion

			#region SalesOrderServiceClient
			{ ErrorCodeEnum.SalesOrderServiceClientGeneral, "Error occured at Sales Order Service client. Please contact Support." },
			#endregion

			#region PurchaseOrderServiceClient
			{ ErrorCodeEnum.PurchaseOrderServiceClientGeneral, "Error occured at Purchase Order Service client. Please contact Support." },
			#endregion

			#region NotificationServiceClient
			{ ErrorCodeEnum.NotificationServiceClientGeneral, "Error occured at Notification Service client. Please contact Support." },
            #endregion

			#region InventoryServiceClient
			{ ErrorCodeEnum.InventoryServiceClientGeneral, "Error occured at Inventory Service client. Please contact Support." },
            #endregion

            #region Sequence
            { ErrorCodeEnum.SequenceError, "Error occured while generating Sequence Numbers.  Please contact support" },
            #endregion

			#region FulfillmentPlanClient
			{ ErrorCodeEnum.FulfillmentServiceClientGeneral, "Error occured at FulfillmentPlan Service client. Please contact Support." },
            #endregion
			
		};

        public static string GetErrorMessage(ErrorCodeEnum errorCode)
        {
            string shortMessage;

            if (_errorCodes.TryGetValue(errorCode, out shortMessage))
            {
                return _errorCodes[errorCode];
            }
            else
            {
                throw new Exception($"No short message for errorcode ({errorCode}).");
            }
        }

        public static Dictionary<ErrorCodeEnum, string> GetAllErrorCodes()
        {
            return _errorCodes;
        }
    }
}
